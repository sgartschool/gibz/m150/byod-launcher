using System;
using System.Collections.Generic;
using System.Text;
using ByodLauncher.Hubs;
using ByodLauncher.Models;
using ByodLauncher.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;

namespace ByodLauncher
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private readonly string LocalhostCorsPolicy = "_localhostCorsPolicy";
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDistributedMemoryCache();

            var connectionString = Configuration.GetConnectionString("MariaDb");
            var serverVersion = ServerVersion.AutoDetect(connectionString);
            services.AddDbContext<ByodLauncherContext>(options => options
                                                                  .UseMySql(connectionString, serverVersion)
                                                                  .EnableSensitiveDataLogging()
                                                                  .EnableDetailedErrors());

            services.AddIdentity<ByodUser, UserRole>()
                    .AddEntityFrameworkStores<ByodLauncherContext>();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(45);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
                options.Cookie.Name = "ByodLaucher";
                options.Cookie.SameSite = SameSiteMode.Strict;
            });

            services.AddAuthentication(options =>
                    {
                        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    })
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = Configuration["Authentication:Jwt:Issuer"],
                            ValidAudience = Configuration["Authentication:Jwt:Audience"],
                            IssuerSigningKey =
                                new SymmetricSecurityKey(Encoding.UTF8
                                                                 .GetBytes(Configuration["Authentication:Jwt:Key"])),
                        };
                    });

            services.AddScoped<SessionCodeService>();

            services.AddCors(options =>
            {
                options.AddPolicy(LocalhostCorsPolicy,
                                  builder =>
                                  {
                                      builder
                                          .WithOrigins(
                                              // "https://localhost:5001",
                                              // "http://localhost:5000",
                                              // "https://localhost:44369",
                                              // "http://localhost:43847",
                                              // "https://byodlauncher.ch",
                                              // "https://www.byodlauncher.ch",
                                              "https://m150.gibz-informatik.ch" // Dev-Login (may be removed in production mode)
                                          )
                                          .AllowAnyHeader()
                                          .AllowAnyMethod()
                                          .AllowCredentials();
                                  });
            });

            services.AddAutoMapper(typeof(Startup));
            services.AddSignalR();
            services.AddSpaStaticFiles(options => { options.RootPath = "wwwroot/dist"; });
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            // app.UseHttpsRedirection();

            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("X-Frame-Options", "SAMEORIGIN");

                Dictionary<string, List<string>> sources = new Dictionary<string, List<string>>
                {
                    ["script-src"] = new List<string>
                        {"'self'", "'nonce-QllPRCBMYXVuY2hlciB2b24gUGV0ZXIgR2lzbGVy'"},
                    ["style-src"] = new List<string>
                    {
                        "'self'", "https://fonts.googleapis.com/", "https://use.fontawesome.com/",
                        "'nonce-QllPRCBMYXVuY2hlciB2b24gUGV0ZXIgR2lzbGVy'"
                    },
                    ["font-src"] = new List<string>
                        {"'self'", "https://fonts.gstatic.com/ https://use.fontawesome.com/"},
                    ["img-src"] = new List<string> {"'self'"},
                    ["frame-ancestors"] = new List<string> {"'none'"},
                    ["connect-src"] = new List<string> {"'self'"},
                    ["default-src"] = new List<string> {"'self'"},
                };

                if (env.IsDevelopment())
                {
                    sources["script-src"].Add("'unsafe-eval'");
                    sources["style-src"].Add("'unsafe-inline'");

                    foreach (var keyValuePair in sources)
                    {
                        keyValuePair.Value.RemoveAll(source => source.StartsWith("'nonce-"));
                    }
                }

                StringBuilder sb = new StringBuilder();

                foreach (KeyValuePair<string, List<string>> entry in sources)
                {
                    sb.Append(entry.Key + " " + string.Join(' ', entry.Value) + "; ");
                }

                context.Response.Headers.Add("Content-Security-Policy", sb.ToString());
                await next();
            });

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseCors(LocalhostCorsPolicy);
            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<SessionHub>("sessionHub");
            });

            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(Configuration["FileUpload:FilesystemAbsolutePath"]),
                RequestPath = Configuration["FileUpload:UriPathSegment"],
                EnableDirectoryBrowsing = false
            });

            app.UseSpaStaticFiles();
            app.UseSpa(builder =>
            {
                if (env.IsDevelopment())
                {
                    builder.UseProxyToSpaDevelopmentServer("http://localhost:8080");
                }
            });
        }
    }
}