import Api from "@/api/api";
import {AxiosError, AxiosResponse} from "axios";
import axiosInstance from "@/api/axios";

class AuthApi extends Api {

    public getJwtFromSessionId(sessionId: string): Promise<string> {
        return this.get<string>(`authentication/getJwtForSessionId`, {params: {sessionId}})
            .then((response: AxiosResponse) => {
                const token = response.data;
                axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
                localStorage.setItem("sessionToken", token);
                return token;
            })
            .catch((error: AxiosError) => {
                throw error;
            });
    }

    public MakeAuthenticatedRequest(): Promise<string> {
        return this.get<string>(`authentication/authenticatedRequest`)
            .then((response: AxiosResponse) => {
                return response.data;
            })
            .catch((error: AxiosError) => {
                throw error;
            });
    }

    public performAuthentication() {
        if (AuthApi.jwtValid()) {
            axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem("sessionToken")}`;
        }
        else {
            window.location.replace("https://m150.gibz-informatik.ch")
        }
    }

    private static jwtValid(): boolean {
        const token = localStorage.getItem("sessionToken");
        if (token === null) {
            return false;
        }
        try {
            const jwtDetail = JSON.parse(atob(token.split('.')[1]));
            return new Date(jwtDetail.exp*1000) > new Date();
        }
        catch (e) {
            console.error("Login failed: " + e);
            return false;
        }
    }

}

export const authApi = new AuthApi();