namespace ByodLauncher.Models
{
    public record JwtTokenConfiguration(string Secret,
                                        string Issuer,
                                        string Audience,
                                        int AccessTokenExpiration,
                                        int RefreshTokenExpiration
    );
}