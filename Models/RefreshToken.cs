using System;
using System.ComponentModel.DataAnnotations;

namespace ByodLauncher.Models
{
    public class RefreshToken
    {
        [Key] public string Token { get; set; }
        public Guid UserId { get; set; }
        public ByodUser User { get; set; }
        public DateTime IssuedAt { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}